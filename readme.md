## DropDown
Using DropDown element in the UI with fixed values.
### Description
This sample shows the usage of DropDown element on a device page. There are two options shown with static entries and values.
1. Options without group. DropDown entries are shown as a list and displayed on the same level. 
2. Options in OptionGroups. DropDown entries are shown grouped in different categories.
### How to Run
This sample can be run on the Emulator or on a device. After starting, the user interface can be seen at the DevicePage in AppStudio or by using a web browser.

### Topics
System, User-Interface, Sample, SICK-AppSpace
